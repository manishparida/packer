# Script to disable WinRM on shutdown

# Delete the HTTP listener
& cmd.exe /c "winrm delete winrm/config/listener?address=*+transport=HTTP"

# Delete the WinRM user
& cmd.exe /c "NET USER m37sjhd78 /DELETE"

# Stop the shutdown script from running on future shutdowns
remove-item "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Group Policy\State\Machine\Scripts\Shutdown" -recurse
remove-item "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Shutdown" -recurse
clear-content C:\Windows\System32\GroupPolicy\Machine\Scripts\psscripts.ini