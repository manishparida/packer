# https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-windows.html
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[Net.ServicePointManager]::SecurityProtocol = "Tls12, Tls11, Tls, Ssl3"
mkdir "C:\Temp"
$dlurl = "https://awscli.amazonaws.com/AWSCLIV2.msi"
$installerPath = "C:\Temp\AWSCLIV2.msi"
Write-Host "Starting Download"
Invoke-WebRequest $dlurl -OutFile $installerPath
Write-Host "Finish Download"
$installerPath = "C:\Temp\AWSCLIV2.msi"
Write-Host "installation Started"
Start-Process -FilePath msiexec -Args "/i $installerPath /passive" -Verb RunAs -Wait
Write-Host "installation Finish"